from locators import WpLoginLocators, WpDashboardLocators, WpMainLocators
from selenium.webdriver.common.keys import Keys
from time import sleep


class BasePage(object):
    def __init__(self, driver):
        self.driver = driver


class WpLoginPage(BasePage):
    def log_in(self, login, password):
        self.driver.find_element(*WpLoginLocators.LOGIN_INPUT).send_keys(login + Keys.ENTER)
        self.driver.find_element(*WpLoginLocators.SUBMIT_BUTTON).click()
        sleep(1)
        self.driver.find_element(*WpLoginLocators.PASSWORD_INPUT).send_keys(password)
        self.driver.find_element(*WpLoginLocators.SUBMIT_BUTTON).click()
        sleep(1)


class WpDashboardPage(BasePage):
    def click_add_new_post(self):
        self.driver.find_element(*WpDashboardLocators.NEW_POST_BUTTON).click()
        sleep(5)

    def input_title(self, title):
        self.driver.find_element(*WpDashboardLocators.TITLE_TEXTAREA).send_keys(title)

    def publish_post(self):
        self.driver.find_element(*WpDashboardLocators.PUBLISH_BUTTON).click()
        sleep(1)
        self.driver.find_element(*WpDashboardLocators.PUBLISH__CONFIRM_BUTTON).click()
        sleep(1)


class WpMainPage(BasePage):
    def is_post_main_page(self, title):
        return self.driver.find_element(*WpMainLocators.get_post_tile(title))