### example-web-service-test

## Launch

Console:
```sh
$ python3 -m unittest start.py
```

Pycharm:
- set cursor on class name in start.py
- click ctrl+shift+f10
