import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import page
from time import sleep
import random
import string

class WpAdminBreaker(unittest.TestCase):
    def log_in(self):
        login_page = page.WpLoginPage(self.driver)
        login_page.log_in("login", "password")

    def setUp(self):
        # self.driver = webdriver.Firefox()  # działa gorzej od Chroma_
        # options = Options()
        # options.add_argument("--headless")
        self.driver = webdriver.Chrome()
        self.driver.get("https://dotestowaniastrona.wordpress.com/wp-admin/")
        self.log_in()

    def test_add_new_post(self):
        dashboard_page = page.WpDashboardPage(self.driver)
        dashboard_page.click_add_new_post()
        title = "Testowy tytuł {}".format(''.join(random.choices(string.ascii_uppercase + string.digits, k=6)))
        dashboard_page.input_title(title)
        dashboard_page.publish_post()
        self.driver.get("https://dotestowaniastrona.wordpress.com/")
        sleep(3)
        blog_page = page.WpMainPage(self.driver)
        assert blog_page.is_post_main_page(title)

    def tearDown(self):
        self.driver.close()


if __name__ == "__main__":
    unittest.main()
