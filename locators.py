from selenium.webdriver.common.by import By


class BaseLocators(object):
    pass


class WpLoginLocators(BaseLocators):
    LOGIN_INPUT = (By.ID, "usernameOrEmail")
    PASSWORD_INPUT = (By.ID, "password")
    SUBMIT_BUTTON = (By.XPATH, "//button[@type='submit']")


class WpDashboardLocators(BaseLocators):
    NEW_POST_BUTTON = (By.XPATH, "//a[@href='https://wordpress.com/post/dotestowaniastrona.wordpress.com' and @class='ab-item']")
    TITLE_TEXTAREA = (By.XPATH, "//textarea")
    PUBLISH_BUTTON = (By.XPATH, "//button[@class='button editor-publish-button is-primary']")
    PUBLISH__CONFIRM_BUTTON = (By.XPATH, "//button[contains(text(), 'Opublikuj')]")


class WpMainLocators(BaseLocators):
    @staticmethod
    def get_post_tile(post_title):
        return By.XPATH, "//span[text()='{}']".format(post_title)
